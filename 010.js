const newstudents = ["Gordon", "Alex", "Jason", "Joan", "Tracy", "Michael"];
// students.slice(2,3);
newstudents.sort();
newstudents.reverse();
console.log(newstudents.indexOf("Alex"));
console.log(newstudents.indexOf("Jason"));
console.log(newstudents);

const students = [
    {"name": "Jason", "age": 23},
    {"name": "Gordon", "age": 30},
    {"name": "Brian", "age": 99},
    {"name": "Alex", "age": 31},
    {"name": "Michael", "age": 32},
]

// Mutation
// array.push, array.pop, array,shift, array.unshift
students.sort(function(studentA,studentB){
    if(studentA.name > studentB.name){
        return 1;
    }else if(studentA.name < studentB.name){
        return -1;
    }else{
        return 0;
    }
});

// Immutation
console.log(students.findIndex(function(student){
    if(student.name === "Alex"){
        return true;
    }else{
        return false;
    }
}));
console.log(students.some(function(student){
    if(student.age > 40){
        return true;
    }
}))
console.log(students.find(function(student){
    if(student.name === "Alex"){
        return true;
    }else{
        return false;
    }
}).age)
// .map .reduce .filter
// .map N input N output
console.log(students.map(function(student){
    return student.age;
}))
// .filter
console.log(students.filter(function(student){
    return (student.age <= 30)
}))

let x = 0;
let qualifiedStudents = [];
while(x < students.length){
    if(students[x].age <= 30){
        qualifiedStudents.push(students[x]);
    }
    x++;
}
console.log(qualifiedStudents);
// .filter .map
console.log(students.filter(function(student){
    return (student.age > 30);
}).map(function(student){
    return student.name;
}))
// .reduce
console.log(students.reduce(function(previous,current){
    return previous + current.age;
},0))

console.log(students.reduce(function(previous,current){
    if(current.age < previous.age){
        return current;
    }else{
        return previous;
    }
},{"name": "No one", "age": 9999}).name)
console.log(students);