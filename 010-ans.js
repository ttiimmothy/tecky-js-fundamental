const weathers = [
    {"date": "1 Jan", "minTemperature": 11, "maxTemperature": 18},
    {"date": "2 Jan", "minTemperature": 10, "maxTemperature": 17},
    {"date": "3 Jan", "minTemperature": 12, "maxTemperature": 17},
    {"date": "4 Jan", "minTemperature": 14, "maxTemperature": 19},
    {"date": "5 Jan", "minTemperature": 16, "maxTemperature": 21},
    {"date": "6 Jan", "minTemperature": 17, "maxTemperature": 21},
    {"date": "7 Jan", "minTemperature": 18, "maxTemperature": 22},
]

// clone
const sortingTemperature = weathers.slice()
console.log(sortingTemperature.sort(function(tempA, tempB){
    return tempB.minTemperature - tempA.minTemperature;
}))
console.log(sortingTemperature.sort(function(date1,date2){
    if(date1.minTemperature < date2.minTemperature){
        return 1;
    }else if(date1.minTemperature > date2.minTemperature){
        return -1;
    }else{
        return 0;
    }
}));

console.log(weathers.map(function(weather){
    return weather.date;
}))
console.log(weathers.filter(function(weather){
    return (weather.maxTemperature > 20);
}).map(function(weather){
    return weather.date;
}))
console.log(weathers.reduce(function(previous,current){
    if(current.minTemperature < previous.minTemperature){
        return current;
    }else{
        return previous;
    }
},{"date": "8 Jan", "minTemperature": 20}).date)