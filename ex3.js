const integer = Math.floor(Math.random() * 11);
let message = document.querySelector('div');
let attempt = 2;
message.innerHTML = integer;
let guess = parseInt(prompt("What's your number?"));
while(attempt > 0){
    if(isNaN(guess) || guess > 10 || guess < 0){
        message.innerHTML = 'You need to input an integer between 0 and 10';
        continue;
    }
    if(guess > integer){
        guess = prompt('Your guess is larger than the answer \nWhat\'s your number?');
        attempt--;
    }else if(guess < integer){
        guess = prompt('Your guess is smaller than the answer \nWhat\'s your number?');
        attempt--;
    }else{
        message.innerHTML = 'You win';
        break;
    }
}
if(attempt == 0){
    message.innerHTML = 'You have used all attempts';
}
