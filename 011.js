let x = 0;
let arr = ["alex", "gordon", "michael"];
while(x < arr.length){
    console.log(arr[x]);
    x++;
}

// Short-hand for while loop
// Very suitable for array
for(let i = 0; i < arr.length; i++){
    console.log(arr[i]);
}
// for-of loop: Array
for(const name of arr){
    console.log(name);
}

// for-in loop: Object
const count = {
    a: 2, b: 1, c: 2, d: 2, e: 1
}
for(const key in count){
    console.log(key + " has been shown for " + count[key] + " times")
}