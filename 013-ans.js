const readlineSync = require("readline-sync")
const add = require("date-fns/add")
const parseISO = require("date-fns/parseISO")
const format = require('date-fns/format')

while(true){
    const startDate = readlineSync.question("Enter the key date: ")
    console.log("100-day celebration: " + format(add(parseISO(startDate), {days:100}),"yyyy-MM-dd"))
    console.log("100-month celebration: " + format(add(parseISO(startDate), {months:100}),"yyyy-MM-dd"))
    console.log("10000000-second celebration: " + format(add(parseISO(startDate), {seconds:10000000}),"yyyy-MM-dd"))
}
