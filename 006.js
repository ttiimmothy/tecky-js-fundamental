const employee = "Chan Tai Man";
console.log("Chan Tai Man");

let x = 1;
while(x < 10){
    console.log(x);
    x = x + 1;
}
console.log("Done!");

let y = 2;
while(y <= 20){
    console.log(y);
    y = y + 2;
}

x = 1;
while(x <= 10){
    console.log(x * 2);
    x = x + 1;
}
// x: 0, 1, 2, 3, 4, 5
// output: 0, 1, 0, 1, 0, 1
x = 0;
while(x < 6){
    console.log(x % 2);
    x = x + 1;
}
// x, star
// 0, *
// 1, **
// 2, ***
// 3, ****
// 4, *****
x = 0;
y = "*";
while(x < 5){
    console.log(y);
    x++;
    y += "*";
}
x = 0;
while(x < 5){
    let star = "*";
    let y = 0;
    while(y < x){
        star = star + "*";
        y = y + 1;
    }
    console.log(star);
    x = x + 1;
}