const readlineSync = require("readline-sync")
const add = require("date-fns/add")
const parseISO = require("date-fns/parseISO")

console.log(add(new Date(), {days: 100}))
const birthday = readlineSync.question("What is your birthday?")
console.log(add(parseISO(birthday), {days:10000}))
const colors = ["blue", "yellow", "black", "white"]
const favouriteColor = readlineSync.keyInSelect(colors, "What is your favourite color?")
console.log("Oops your favourite color is " + colors[favouriteColor])

const weight = readlineSync.question("What is your weight?")
const height = readlineSync.question("How tall are you?")
console.log(parseInt(weight) / Math.pow(parseFloat(height), 2))