// range(10)
// range(5)
function range(num){
    let array = [];
    for(let i = 0; i < num; i++){
        array.push(i);
    }
    return array;
}
function bonus(salary, year, performance){
    if(year > 1 && year <= 5){
        return salary;
    }else if(year > 5 && performance){
        return salary * 2;
    }else if(year > 5){
        return salary;
    }
    return 0;
}
console.log(range(5));
console.log(bonus(10000, 6, "poor"));
console.log(bonus(10000, 6, false) + bonus(40000, 5.5, true) + bonus(80000, 3, true) + bonus(70000, 7, true) + bonus(20000, 0,5, true));