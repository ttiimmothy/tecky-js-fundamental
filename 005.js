// console.log(true)
// console.log(false)

// AND Operator
// console.log(true && true)
// console.log(true && false)
// console.log(false && true)
// console.log(false && false)

// OR Operator
console.log(true || true)
console.log(true || false)
console.log(false || true)
console.log(false || false)

console.log((true || false) && true)
console.log(true || false && true)

// NOT operator
console.log(!true)
console.log(!!false)

// Comparison operator
console.log(5 > 5)
console.log(4 < 5)
console.log(5 >= 1)
console.log(1 + 1 <= 2)

const name = "May"
console.log(name == "Alex")
console.log(name != "Alex")

// Flow Control - Condition
if(name == "Alex"){
    console.log("Hi, Alex")
    console.log("This is today's menu. What would you want to have?")
}else if(name == "Peter"){
    console.log("Good morning, Boss") 
}else{
    console.log("Excuse Sir. Do you have a reservation?")
}