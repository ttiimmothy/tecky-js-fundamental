let randomList = [];
let i = 0, x = 0, y = 0;
for(i = 0; i < 10; i++){
    randomList.push(Math.floor(Math.random() * 20) + 1);
}
console.log(randomList);
let newList = [];
for(x = 0; x < randomList.length; x++){
    if(randomList[x] % 2 == 0){
        newList.push(randomList[x]);
    }else{
        newList.unshift(randomList[x]);
    }
}
console.log(newList);
let substitute = 0;
for(x = 0; x < randomList.length; x++){
    for(y = 0; y < randomList.length; y++){
        if(x < y){
            if(randomList[x] > randomList[y]){
                substitute = randomList[x];
                randomList[x] = randomList[y];
                randomList[y] = substitute;
            }
        }else if(x > y){
            if(randomList[x] < randomList[y]){
                substitute = randomList[x];
                randomList[x] = randomList[y];
                randomList[y] = substitute;
            }  
        }
    }
}
console.log(randomList);
// Bubble sorting
let numList = [];
for(i = 0; i < 10; i++){
    numList.push(Math.floor(Math.random() * 20) + 1);
}
console.log(numList);
substitute = 0;
for(i = 0; i < numList.length; i++){
    for(let j = 0; j < numList.length; j++){
        if(numList[j] > numList[j+1]){
            substitute = numList[j];
            numList[j] = numList[j + 1];
            numList[j + 1] = substitute;
        }
    }
}
console.log(numList)

let arr = [];
for(i = 0; i < 10; i++){
    arr.push(Math.floor(Math.random() * 20) + 1);
}
console.log(arr)
function bubbleSort(arr) {
    const sortedArr = arr;
    let swapped = true;
    while(swapped){
        swapped = false
        for (let i = 1; i < sortedArr.length; ++i) {
            if (sortedArr[i - 1] > sortedArr[i]) {
            [sortedArr[i], sortedArr[i - 1]] = [sortedArr[i - 1], sortedArr[i]];
            swapped = true;
            }
        }
    }
    return sortedArr;
}
console.log(bubbleSort(arr))