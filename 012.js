// let count
// block scope

// var
// function scope
const a = 0;
if(2 > 1){
   let a = 0;
   a = a + 1;
   console.log(a);
   if(4 > 3){
       const a = 2;
       console.log(a);
   }
}
for(let a = 0; a < 3; a++){
    console.log("for " + a);
}
function abc(){
    let a = 99;
    console.log(a);
}
abc();
console.log(a);

var ab = 0;
function abc(){
    var ab = 0;
    ab = ab + 1;
    console.log(ab);
}
abc();
console.log(ab);
let b = 0;
if(2 > 1){
    let b = 0;
    b = b + 1;
    console.log(b);
}
console.log(b);

function abc(){
    let count = 0;
    return function(){
        count = count + 1;
        console.log(count);
    }
}
const returnedFunction = abc();
returnedFunction();
returnedFunction();
returnedFunction();
const anotherAbc = abc();
anotherAbc();
anotherAbc();

function createCounter(){
    let count = 0;
    return{
        add: function(){
            count = count + 1;
        },
        result: function(){
            return count;
        }
    }
}
const counter1 = createCounter();
counter1.add();
counter1.add();
const counter2 = createCounter();
counter1.add();
counter2.add();
console.log(counter1.result());
console.log(counter2.result());