console.log("text")

const lastCharacter = "text".substring(3)

console.log(Math.max(1, 2, 3, 4, 5))
console.log(Math.min(1, 2, 3, 4, 5))

console.log(Math.floor(1.05))
console.log(Math.ceil(1.05))
console.log(Math.round(1.55))
console.log(Math.round(27999 / 1000) * 1000)

const first = 27369
console.log(Math.floor(first / 60))

console.log(Math.floor(Math.random() * 3))