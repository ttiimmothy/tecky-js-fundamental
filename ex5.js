function rnaTranscription(input){
    let newString = ''
    for(let char of input){
        if(char == 'G'){
            newString += 'C'
        }else if(char == 'C'){
            newString += 'G'
        }else if(char == ' T'){
            newString += 'A'
        }else{
            newString += 'U'
        }
    }
    return newString
}
console.log(rnaTranscription("GCTAGCT"))