//Object
const alex = {
    "name": "Alex",
    "gender": "M",
    "height": 170,
    "height": 180,
    "living-place": "Hong Kong"
}
console["log"](alex["height"])
console.log(alex.height)
console.log(alex["living-place"])


const students = [
    {"name": "Alex", "height": 170},
    {"name": "Gordon", "height": 180}
]
students[0].height = 172
students[1]["living-place"] = "Hong Kong"
delete students[1]["living-place"]
console.log(students[0].height)
console.log(students[0]["height"])
console.log(students)

const checkDuplicate = {}
const chars = ["a", "b", "a", "c", "d", "e", "d"]
let x = 0
while (x < chars.length){
    if(checkDuplicate[chars[x]] == null){
        checkDuplicate[chars[x]] = 0
    }
    checkDuplicate[chars[x]] = checkDuplicate[chars[x]] + 1
    console.log(checkDuplicate)
    x = x + 1
}
console.log(checkDuplicate)