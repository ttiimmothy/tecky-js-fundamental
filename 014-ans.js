const fs = require("fs");
const readLineSync = require("readline-sync");
const taskList = fs.readFileSync("014-task.txt", "utf-8").split("\n");
console.log("You have below tasks");
for(let i = 0; i < taskList.length; i++){
    console.log(`[${i + 1}] ${taskList[i]}`);
}
const ask = readLineSync.question(`If you want to add a task, type `+`.  If you want to clear a task, type the number (1-${taskList.length}): `);
if(ask === "+"){
    const addTask = readLineSync.question("What task is it? ");
    taskList.push(addTask);
    fs.writeFileSync("014-task.txt", taskList.join("\n"));
}else{
    const clearTask = parseInt(ask);
    if(!isNaN(clearTask)){
        taskList.splice(clearTask - 1,1);
        fs.writeFileSync("014-task.txt", taskList.join("\n"));
    }
}